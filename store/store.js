import Vue from 'vue';
import Vuex from 'vuex';
import moduleCart from './cart'
import moduleUser from './user'
// 把Vuex当成插件安装在Vue实例
Vue.use(Vuex);

const store = new Vuex.Store({
     // 购物车   用户信息 
     modules:{
       m_cart:moduleCart,
       m_user:moduleUser
     }
});

export default store;