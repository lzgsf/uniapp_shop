export default {
   namespaced: true,
   state:()=>{
      return {
         address:JSON.parse(uni.getStorageSync('address') ||'{}')
      }
   },
   mutations:{
      saveAddressToStorage (state) {
         uni.setStorageSync('address', JSON.stringify(state.address));
      },
      updateAddress (state, address) {
         state.address = address;
         this.commit('m_user/saveAddressToStorage')
      }
   },
   getters:{
     addStr (state) {
        const {provinceName,cityName,countyName,detailInfo} = state.address;
        if (!provinceName) return;
        return [provinceName, cityName ,countyName ,detailInfo].join('')
     }
   }
}