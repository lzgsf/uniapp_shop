
// #ifndef VUE3
import Vue from 'vue'
import App from './App'
import {$http} from '@escook/request-miniprogram';
import store from '@/store/store.js'  // @ 表示根目录
 
 // npm init --y  创建package.json 
 // npm i @escook/request-miniprogram
 
 uni.$http = $http;
 
 //设置基础请求地址
 // $http.baseUrl = "https://www.uinav.com";
 $http.baseUrl = "https://api-hmugo-web.itheima.net";
 
 // 请求之前做些事
 $http.beforeRequest = function (options) {
     uni.showLoading({
        title:"数据加载种..."
     });
 }
 // 请求之后做些事
 $http.afterRequest = function () {
     uni.hideLoading();
 }
 
 // 封装数据加载失败的弹框提示
 uni.$showMsg = function (title="数据加载失败!", duration=1500) {
   uni.showToast({
     title,
     duration,
     icon:"none"
   })
 }
 
Vue.config.productionTip = false

App.mpType = 'app'

const app = new Vue({
    ...App,
    store
})
app.$mount()
// #endif

// #ifdef VUE3
import { createSSRApp } from 'vue'
import App from './App.vue'
export function createApp() {
  const app = createSSRApp(App)
  return {
    app
  }
}
// #endif